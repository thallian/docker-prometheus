Monitoring with [Prometheus](https://prometheus.io/).

At the moment it monitors itself and [cAdvisor](https://github.com/google/cadvisor) endpoints.

# Volumes
- `/var/lib/prometheus/data`

# Environment Variables
## SCRAPE_INTERVAL
- default: 1m

How frequently to scrape targets.

## SCRAPE_TIMEOUT
- default: 10s

How long until a scrape request times out.

## EVALUATION_INTERVAL
- default: 1m

How frequently to evaluate rules.

## EXTERNAL_LABELS
- default: monitor=docker-monitor

Attach these labels to any time series or alerts when communicating with external systems.

Key-Value pairs are seperated through `;`.

## MEMORY_CHUNKS
- default: 1048576

How many chunks to keep in memory. While the size of a chunk is 1kiB, the total
memory usage will be significantly higher than this value * 1kiB. Furthermore,
for various reasons, more chunks might have to be kept in memory temporarily.

## CADVISOR_ENDPOINTS

Comma seperated list of cAdvisor endpoints to scrape.

# Ports
- 9090

# Capabilities
- CHOWN
- DAC_OVERRIDE
- FOWNER
- NET_BIND_SERVICE
- SETGID
- SETUID
