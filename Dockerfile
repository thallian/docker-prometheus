FROM quay.io/thallian/confd-env:latest

ENV VERSION 1.5.2

RUN addgroup -g 2222 prometheus
RUN adduser -h /var/lib/prometheus -u 2222 -D -G prometheus prometheus

RUN apk add --no-cache libressl

RUN wget -qO- https://github.com/prometheus/prometheus/releases/download/v$VERSION/prometheus-$VERSION.linux-amd64.tar.gz | tar -xz -C /var/lib/prometheus --strip 1

ADD /rootfs /

RUN chown -R prometheus:prometheus /var/lib/prometheus

EXPOSE 9090

VOLUME /var/lib/prometheus/data
